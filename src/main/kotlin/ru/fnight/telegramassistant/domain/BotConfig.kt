package ru.fnight.telegramassistant.domain

class BotConfig {

    var url: String = ""
    var token: String = ""
    var name: String = ""

}