package ru.fnight.telegramassistant.domain

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Scope
import org.springframework.stereotype.Component
import org.telegram.telegrambots.bots.TelegramLongPollingBot
import org.telegram.telegrambots.meta.api.methods.send.SendMessage
import org.telegram.telegrambots.meta.api.objects.Update
import ru.fnight.telegramassistant.service.TestService

@Component
@Scope("prototype")
class TelegramBot constructor(var options: BotOptions) : TelegramLongPollingBot(options) {

    @Autowired
    lateinit var testService: TestService

    override fun getBotUsername(): String {
        return options.config?.name ?: ""
    }

    override fun getBotToken(): String {
        return options.config?.token ?: ""
    }

    override fun onUpdateReceived(update: Update) {
        if (update.hasMessage() && update.message.hasText()) {
            val message = SendMessage()
                    .setChatId(update.message.chatId)
                    .setText(testService.test())

            execute(message)
        }
    }
}