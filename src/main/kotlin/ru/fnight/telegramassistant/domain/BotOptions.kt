package ru.fnight.telegramassistant.domain

import org.telegram.telegrambots.bots.DefaultBotOptions

class BotOptions: DefaultBotOptions() {

    var config: BotConfig? = null

    fun addConfig(config: BotConfig) {
        this.config = config
        baseUrl = config.url
    }

}