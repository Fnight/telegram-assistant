package ru.fnight.telegramassistant.service.impl

import org.springframework.stereotype.Service
import ru.fnight.telegramassistant.service.TestService

@Service
class TestServiceImpl : TestService {

    override fun test(): String {
        return "Privet"
    }

}