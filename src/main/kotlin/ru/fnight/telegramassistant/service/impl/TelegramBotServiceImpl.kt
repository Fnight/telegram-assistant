package ru.fnight.telegramassistant.service.impl

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.ApplicationContext
import org.springframework.context.annotation.Configuration
import org.springframework.stereotype.Service
import org.telegram.telegrambots.ApiContextInitializer
import org.telegram.telegrambots.meta.ApiContext
import org.telegram.telegrambots.meta.TelegramBotsApi
import ru.fnight.telegramassistant.domain.AppConfig
import ru.fnight.telegramassistant.domain.BotOptions
import ru.fnight.telegramassistant.domain.TelegramBot
import ru.fnight.telegramassistant.service.TelegramBotService
import java.io.File
import java.io.IOException
import javax.annotation.PostConstruct

@Service
@Configuration
open class TelegramBotServiceImpl : TelegramBotService {

    val bots = ArrayList<TelegramBot>()
    val api = TelegramBotsApi()

    @Autowired
    lateinit var context: ApplicationContext

    @PostConstruct
    fun init() {
        ApiContextInitializer.init()

        val botOptions = ApiContext.getInstance(BotOptions::class.java)

        val mapper = ObjectMapper(YAMLFactory())
        try {
            mapper.readValue<AppConfig>(File("config.yml"), AppConfig::class.java)?.let {
                if (it.botConfig != null) {
                    botOptions.addConfig(it.botConfig!!)
                }
            }
        } catch (e: IOException) {
            e.printStackTrace()
        }

        bots.add(context.getBean(TelegramBot::class.java, botOptions))

        for (bot in bots) {
            api.registerBot(bot)
        }
    }

    override fun getTelegramBots(): ArrayList<TelegramBot> {
        return bots
    }

    override fun getTelegramApi(): TelegramBotsApi {
        return api
    }
}