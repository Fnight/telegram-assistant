package ru.fnight.telegramassistant.service

import org.telegram.telegrambots.meta.TelegramBotsApi
import ru.fnight.telegramassistant.domain.TelegramBot

interface TelegramBotService {

    fun getTelegramBots(): ArrayList<TelegramBot>
    fun getTelegramApi(): TelegramBotsApi

}