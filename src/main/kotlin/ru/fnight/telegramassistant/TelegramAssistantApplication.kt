package ru.fnight.telegramassistant

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication

@SpringBootApplication
open class TelegramAssistantApplication {
    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            SpringApplication.run(TelegramAssistantApplication::class.java, *args)
        }
    }
}